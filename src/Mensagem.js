import React, {Component} from 'react';
import User from './User';
import photo from './batman.png';

class Mensagem extends Component {
    render(){
        return (
            <div>
              Mensagem Amigável para ajudar vocês.
              <User name="Jeovah" photo={photo}></User>
            </div>
        );
    }
}

export default Mensagem;
