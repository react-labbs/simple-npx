import React, {Component} from 'react';

class Calculadora extends Component {

  constructor(props){
    super(props)
    this.state = {
      horario1: "0800",
      horario2: "1000",
      horario3: "1030",
      horario4: "1200",
      resultado: 0,
      resultadoString: "string",
      mensagem: "Sucesso."
    }
    this.changeState = this.changeState.bind(this)
    this.resetState = this.resetState.bind(this)
    this.changeInput = this.changeInput.bind (this)

  }

  changeState(){
    this.calculateTime()
    this.setState({
      mensagem: "Sucesso."
    })
  }

  resetState(){
    this.setState({
      horario1: "0000",
      horario2: "0000",
      horario3: "0000",
      horario4: "0000"
    })
  }

  changeInput(event){
    
    let target = event.target
    let index = target.name
    this.setState({
      [index]:target.value
    })
  }

  calculateTime(){

    let hora1 = parseInt(this.state.horario1.substring(0,2), 10);
    let hora2 = parseInt(this.state.horario2.substring(0,2), 10);
    let hora3 = parseInt(this.state.horario3.substring(0,2), 10);
    let hora4 = parseInt(this.state.horario4.substring(0,2), 10);
    
    let minuto1 = parseInt(this.state.horario1.substring(2,4), 10);
    let minuto2 = parseInt(this.state.horario2.substring(2,4), 10);
    let minuto3 = parseInt(this.state.horario3.substring(2,4), 10);
    let minuto4 = parseInt(this.state.horario4.substring(2,4), 10);

    // Converte a quantidade de horas em minutos
    let horas =  (hora2 - hora1) * 60 + (hora4 - hora3) * 60;

    let minutos = minuto2 - minuto1 + (minuto4 - minuto3);

    this.state.resultado = horas + minutos;

    let horasString = Math.floor(parseFloat(this.state.resultado)/60.0);

    let minutosString = this.state.resultado - 60 * horasString;

    this.state.resultadoString = horasString.toString() + "h" + minutosString.toString();

  }

  render(){

    return (
      <div className="App">
        
        Resultado: {this.state.resultadoString} - Total em Minutos: {this.state.resultado}

        <form>
            <div>
                <label>Entrada
                    <input
                    type="text"
                    name="horario1"
                    value={this.state.horario1}
                    onChange={this.changeInput}
                    ></input>
                </label>  
            </div>
            <div>
                <label>Saída para Almoço
                    <input
                    type="text"
                    name="horario2"
                    value={this.state.horario2}
                    onChange={this.changeInput} 
                    ></input>
                </label>
            </div>
            <div>
                <label>Retorno para Almoço
                    <input
                    type="text"
                    name="horario3"
                    value={this.state.horario3}
                    onChange={this.changeInput} 
                    ></input>
                </label>
            </div>
            <div>
                <label>Saída Definitiva
                    <input
                    type="text"
                    name="horario4"
                    value={this.state.horario4}
                    onChange={this.changeInput} 
                    ></input>
                </label>
            </div>
        </form>

        <button onClick={this.changeState}>Calcular</button>  
        <button onClick={this.resetState}>Resetar</button>   

      </div>

    );
  }
}

export default Calculadora;
