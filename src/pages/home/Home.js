import React, {Component} from 'react';

class Home extends Component {

  constructor(props){
    super(props)
    this.state = {
      name: "Jeovah Simoes",
      value: "jeovahfialho@gmail.com"
    }
    this.changeState = this.changeState.bind(this)
    this.resetState = this.resetState.bind(this)
    this.changeInput = this.changeInput.bind (this)

  }

  changeState(){
    this.setState({
      name: "Jeovah Fialho Change"
    })
  }

  resetState(){
    this.setState({
      name: "Jeovah"
    })
  }

  changeInput(event){
    let target = event.target
    let index = target.name
    this.setState({
      [index]:target.value
    })
  }

  render(){

    return (
      <div className="App">
        
        <form>
          <label>Nome
            <input
              type="text"
              name="name"
              value={this.state.name}
              onChange={this.changeInput}
            ></input>
          </label>  
          <label>Email
            <input
              type="text"
              name="email"
              value={this.state.email}
              onChange={this.changeInput} 
            ></input>
          </label>
        </form>

        This State Name: {this.state.name} - {this.state.email}
        <button onClick={this.changeState}>Mudar Estado</button>  
        <button onClick={this.resetState}>Resetar</button>   

      </div>

    );
  }
}

export default Home;
