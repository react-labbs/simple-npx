import Home from "./pages/home/Home";
import User from "./pages/user/User";
import Calculadora from "./pages/calculadora/Calculadora";

const routesConfig = [
    {
        path: "/",
        component: Home,
        exact:true
    },
    {
        path: "/user",
        component: User,
        exact:true
    },
    {
        path: "/calculadora",
        component: Calculadora,
        exact:true
    },
]

export default routesConfig;